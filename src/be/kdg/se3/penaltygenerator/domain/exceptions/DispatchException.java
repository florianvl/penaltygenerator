package be.kdg.se3.penaltygenerator.domain.exceptions;

/**
 * Wrapper for anything that goes wrong when sending the generated violations away.
 */
public class DispatchException extends Exception {
    public DispatchException(String message) {
        super(message);
    }

    public DispatchException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
