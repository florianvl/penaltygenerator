package be.kdg.se3.penaltygenerator.domain.exceptions;

/**
 * Wrapper for problems that occur when generating new Violations.
 */
public class GeneratorException extends Exception{
    public GeneratorException(String s) {
        super(s);
    }
}
