package be.kdg.se3.penaltygenerator.domain.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("EmissionViolation")
public class EmissionViolation extends Violation {
    private final int minimumEuronorm;
    private final int vehicleEuronorm;

    public EmissionViolation(String numberPlate, String streetName, String cityName, int minimumEuronorm, int vehicleEuronorm) {
        super(numberPlate, streetName, cityName);
        this.minimumEuronorm = minimumEuronorm;
        this.vehicleEuronorm = vehicleEuronorm;
    }

    public int getMinimumEuronorm() {
        return minimumEuronorm;
    }

    public int getVehicleEuronorm() {
        return vehicleEuronorm;
    }

    @Override
    public String toString() {
        return String.format("Emission violation by %s, measured %d in %d zone. ", super.getNumberPlate(), vehicleEuronorm, minimumEuronorm);

    }
}
