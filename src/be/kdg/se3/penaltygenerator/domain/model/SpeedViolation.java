package be.kdg.se3.penaltygenerator.domain.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("SpeedViolation")
public final class SpeedViolation extends Violation {
    private final int maxSpeed;
    private final int measuredSpeed;


    public SpeedViolation(String numberPlate, String streetName, String cityName, int maxSpeed, int measuredSpeed) {
        super(numberPlate, streetName, cityName);
        this.maxSpeed = maxSpeed;
        this.measuredSpeed = measuredSpeed;
    }


    public int getMaxSpeed() {
        return maxSpeed;
    }

    public int getMeasuredSpeed() {
        return measuredSpeed;
    }

    @Override
    public String toString() {
        return String.format("Speed violation by %s, measured %d in %d zone. ", super.getNumberPlate(), measuredSpeed, maxSpeed);

    }
}
