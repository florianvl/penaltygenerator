package be.kdg.se3.penaltygenerator.generator;

import java.util.Random;

/**
 * Class responsible for generating Random Strings, with a customizeable maximum length.
 */
public class RandomString {

    private final char[] symbols;
    private final char[] buffer;
    private int MAX_LENGTH;
    private final Random random = new Random();


    public RandomString(int maxPossibleStringLength) {
        if (maxPossibleStringLength < 1) {
            throw new IllegalArgumentException("Length can not be lower than 1.");
        }

        StringBuilder strBuilder = new StringBuilder();

        for (char ch = 'a'; ch <= 'z'; ch++) {
            strBuilder .append(ch);
        }
        symbols = strBuilder .toString().toCharArray();


        buffer = new char[maxPossibleStringLength];
        this.MAX_LENGTH = maxPossibleStringLength;
    }


    public String nextString() {
        for (int i = 0; i < buffer.length; i++) {
            buffer[i] = symbols[random.nextInt(symbols.length)];
        }
        return new String(buffer).substring(0, random.nextInt(MAX_LENGTH) +1 );
    }

}