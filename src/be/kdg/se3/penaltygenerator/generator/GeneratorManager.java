package be.kdg.se3.penaltygenerator.generator;

import be.kdg.se3.penaltygenerator.dispatcher.ViolationDispatcher;
import be.kdg.se3.penaltygenerator.domain.exceptions.GeneratorException;
import be.kdg.se3.penaltygenerator.domain.model.Violation;
import be.kdg.se3.penaltygenerator.dispatcher.XMLConverter;
import be.kdg.se3.penaltygenerator.domain.exceptions.DispatchException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Class responsible for creating a new random Violation every x seconds.
 * X is a random value between two thresholds and changes after every newly generated Violation.
 */
public class GeneratorManager {

    private ViolationGenerator generator;
    private int lowerFrequencyBound;
    private int upperFrequencyBound;

    private ViolationDispatcher dispatcher;
    private XMLConverter xmlConverter;
    private static Logger logger = LogManager.getLogger(GeneratorManager.class);

    public GeneratorManager(ViolationGenerator generator, ViolationDispatcher dispatcher, XMLConverter xmlConverter, int lowerFrequencyBound, int upperFrequencyBound) {
        this.generator = generator;
        this.dispatcher = dispatcher;
        this.lowerFrequencyBound = lowerFrequencyBound;
        this.upperFrequencyBound = upperFrequencyBound;
        this.xmlConverter = xmlConverter;

    }

    public void start() throws DispatchException, GeneratorException {
        logger.log(Level.INFO, "Generator started.");

        while(true){
            Violation violation = generator.generateNewViolation();
            dispatcher.sendViolation(xmlConverter.ViolationToString(violation));
            int timeToSleep = ThreadLocalRandom.current().nextInt(lowerFrequencyBound, upperFrequencyBound +1);
            try {
                Thread.sleep(timeToSleep);
            } catch (InterruptedException e) {
                logger.log(Level.ERROR, "Thread.sleep() interrupted.");
                Thread.currentThread().interrupt();
            }
        }
    }
}
