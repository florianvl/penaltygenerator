package be.kdg.se3.penaltygenerator.generator;

import be.kdg.se3.penaltygenerator.Application;
import be.kdg.se3.penaltygenerator.domain.exceptions.GeneratorException;
import be.kdg.se3.penaltygenerator.domain.model.SpeedViolation;
import be.kdg.se3.penaltygenerator.domain.model.Violation;
import be.kdg.se3.penaltygenerator.domain.model.EmissionViolation;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class responsible for creating new Violations, with random attributes.
 */
public class ViolationGenerator {
    private double chanceOfSpeedViolation;
    private int[] maxSpeeds;
    private int maxLimitForMeasuredSpeeds;
    private String[] numberPlates;
    private String[] cities;
    private int[] euronorms;
    private RandomString randomStringGenerator;
    private static Logger logger = LogManager.getLogger(ViolationGenerator.class);
    private Map<String, Integer> violationTypeChances;
    private int violationChanceTotal = 0;

    public ViolationGenerator(Map<String, Integer> speedViolationChance, int[] maxSpeeds, int maxLimitForMeasuredSpeeds, String[] numberPlates, String[] cities, int[] euronorms, int maxLengthOfNumberplates) {
        this.violationTypeChances = speedViolationChance;
        for (Map.Entry<String, Integer> entry : violationTypeChances.entrySet()) {
            violationChanceTotal = +entry.getValue();
        }

        this.maxSpeeds = maxSpeeds;
        this.maxLimitForMeasuredSpeeds = maxLimitForMeasuredSpeeds;
        this.numberPlates = numberPlates;
        this.cities = cities;
        this.euronorms = euronorms;
        randomStringGenerator = new RandomString(maxLengthOfNumberplates);


    }

    public Violation generateNewViolation() throws GeneratorException {
        String randomStreet = randomStringGenerator.nextString();
        String randomCity = cities[ThreadLocalRandom.current().nextInt(cities.length)];
        String randomNumberPlate = numberPlates[ThreadLocalRandom.current().nextInt(numberPlates.length)];

        Violation newViolation;

        int randomInt = ThreadLocalRandom.current().nextInt(violationChanceTotal);
        String violationTypeToGenerate = "";
        for (Map.Entry<String, Integer> entry : violationTypeChances.entrySet()) {
            randomInt -= entry.getValue();
            if (randomInt <= 0) {
                violationTypeToGenerate = entry.getKey();
                break;
            }
        }
        logger.log(Level.INFO, "Generating: " + violationTypeToGenerate);
        switch (violationTypeToGenerate) {
            case "SpeedViolation":
                int randomMaxSpeed = maxSpeeds[ThreadLocalRandom.current().nextInt(maxSpeeds.length)];
                int measuredSpeed = ThreadLocalRandom.current().nextInt(randomMaxSpeed + 1, maxLimitForMeasuredSpeeds);
                newViolation = new SpeedViolation(randomNumberPlate, randomStreet, randomCity, randomMaxSpeed, measuredSpeed);
                return newViolation;
            case "EmissionViolation":
                int randomMinEuronorm = euronorms[ThreadLocalRandom.current().nextInt(euronorms.length)];
                int measuredEuronorm = ThreadLocalRandom.current().nextInt(1, randomMinEuronorm);
                newViolation = new EmissionViolation(randomNumberPlate, randomStreet, randomCity, randomMinEuronorm, measuredEuronorm);
                return newViolation;

        }

        throw new GeneratorException("No violation generated. Check Map with violation type chances.");
    }


}
