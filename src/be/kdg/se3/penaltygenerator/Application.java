package be.kdg.se3.penaltygenerator;

import be.kdg.se3.penaltygenerator.dispatcher.ViolationDispatcher;
import be.kdg.se3.penaltygenerator.dispatcher.RabbitMqDispatcher;
import be.kdg.se3.penaltygenerator.dispatcher.XMLConverter;
import be.kdg.se3.penaltygenerator.domain.exceptions.GeneratorException;
import be.kdg.se3.penaltygenerator.generator.GeneratorManager;
import be.kdg.se3.penaltygenerator.generator.ViolationGenerator;
import be.kdg.se3.penaltygenerator.domain.exceptions.DispatchException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is responsible for the initialisation and wiring of all necessary objects
 * and for starting the generation of new Violations by starting the scheduler.
 */
public class Application {

    private static String rabbitMqQueueName = "ViolationQueue";
    private static String rabbitMqHost = "localhost";

    private static int frequencyLowerBound = 3000;
    private static int frequencyUpperBound = 3000;

    private static int maxLengthOfNumberplates = 100;
    private static int maxLimitForMeasuredSpeeds = 150;
    private static int[] maxSpeeds = {30, 50, 70, 90, 120};
    private static int[] euronorms = {3, 4, 5, 6};
    private static String[] numberPlates = {"1-KEA-123", "3-XYZ-789", "1-MAC-852", "8-UTT-753", "8-BOY-159", "1-RAK-999", "1-RAN-421", "1-ABC-123", "1-ERR-123", "2-ERR-123", "1-UNK-123", "BAD-PL8"};
    private static String[] cities = {"Antwerp", "Ghent", "Brussels", "Bucharest", "Budapest", "London", "Paris", "Rouen", "New York", "Toronto"};

    private static Map<String, Integer> violationTypeChances = new HashMap<String, Integer>();
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) throws DispatchException, InterruptedException {
        violationTypeChances.put("SpeedViolation", 70);
        violationTypeChances.put("EmissionViolation", 30);
        ViolationGenerator penaltyGenerator = new ViolationGenerator(violationTypeChances, maxSpeeds, maxLimitForMeasuredSpeeds, numberPlates, cities, euronorms, maxLengthOfNumberplates);
        ViolationDispatcher dispatcher = new RabbitMqDispatcher(rabbitMqQueueName, rabbitMqHost);
        XMLConverter xmlConverter = new XMLConverter();
        GeneratorManager scheduler = new GeneratorManager(penaltyGenerator, dispatcher, xmlConverter, frequencyLowerBound, frequencyUpperBound);


        try {
            scheduler.start();
        } catch (GeneratorException e) {
            logger.log(Level.ERROR, e.getMessage());
        }

    }
}
