package be.kdg.se3.penaltygenerator.dispatcher;

import be.kdg.se3.penaltygenerator.domain.model.EmissionViolation;
import be.kdg.se3.penaltygenerator.domain.model.SpeedViolation;
import be.kdg.se3.penaltygenerator.domain.model.Violation;
import com.thoughtworks.xstream.XStream;

/**
 * This class is responsible for the conversion between XML and Violation objects.
 */

public class XMLConverter implements ViolationConverter {
    private XStream xstream;

    public XMLConverter(){
        xstream = new XStream();
        xstream.ignoreUnknownElements();
        xstream.processAnnotations(Violation.class);
        xstream.processAnnotations(SpeedViolation.class);
        xstream.processAnnotations(EmissionViolation.class);    }

    @Override
    public Violation StringToViolation(String violationString) {
        return (Violation) xstream.fromXML(violationString);
    }

    @Override
    public String ViolationToString(Violation violationToConvert) {
        return xstream.toXML(violationToConvert);
    }
}
