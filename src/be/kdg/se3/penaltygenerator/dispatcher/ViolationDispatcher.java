package be.kdg.se3.penaltygenerator.dispatcher;

import be.kdg.se3.penaltygenerator.domain.exceptions.DispatchException;

/**
 * Interface for sending the generated and converted Violations.
 */
public interface ViolationDispatcher {
    public void sendViolation(String convertedViolation) throws DispatchException;
}
