package be.kdg.se3.penaltygenerator.dispatcher;

import be.kdg.se3.penaltygenerator.domain.model.Violation;

/**
 * An interface for conversion of json/xml/... Strings and Violations.
 */
public interface ViolationConverter  {
    public Violation StringToViolation(String violationString);
    public String ViolationToString(Violation violationToConvert);
}
