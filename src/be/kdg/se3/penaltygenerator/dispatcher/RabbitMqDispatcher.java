package be.kdg.se3.penaltygenerator.dispatcher;

import be.kdg.se3.penaltygenerator.domain.exceptions.DispatchException;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * This class is responsible for sending generated Violations on the RabbitMq message queue.
 */

public class RabbitMqDispatcher implements ViolationDispatcher {
    private static Logger logger = LogManager.getLogger(RabbitMqDispatcher.class);
    private String queueName;

    ConnectionFactory factory;
    Connection connection;
    Channel channel;

    public RabbitMqDispatcher(String queueName, String host) throws DispatchException {
        this.queueName = queueName;
        try {
            factory = new ConnectionFactory();
            factory.setHost(host);
            connection = factory.newConnection();
            channel = connection.createChannel();
            channel.queueDeclare(queueName, false, false, false, null);
            logger.log(Level.INFO, "RabbitMq connection successfully set up.");
        } catch (IOException e) {
            logger.log(Level.ERROR, "Error occured at RabbitMq channel setup.");
            throw new DispatchException("Error occured at RabbitMq channel setup.", e);
        } catch (TimeoutException e) {
            logger.log(Level.ERROR, "Error occured when creating RabbitMq connection.");
            throw new DispatchException("Error occured when creating RabbitMq connection.", e);
        }
    }



    @Override
    public void sendViolation(String convertedViolation) throws DispatchException {
        try {
            channel.basicPublish("", queueName, null, convertedViolation.getBytes());
            logger.log(Level.INFO, "Violation successfully sent on " + queueName);
        } catch (IOException e) {
            logger.log(Level.ERROR, "Error occured when publishing message on queue " + queueName);
            throw new DispatchException("Error occured when publishing message on queue " + queueName, e);
        }

    }


}
